# Web Servers

Ansible code to provision [CRIN](https://www.crin.org/) web servers.

The previous documentation is available on the [archive of the old Trac site](https://trac.crin.org.archived.website/).

To configure the dev Stretch server (note that Cloudflare is in use):

```bash
export SERVERNAME="crin4.webarchitects.co.uk"
export INTERNALIP=$(dig ${SERVERNAME} +short)
export PRODDOMAIN="crin.site"
export EXTERNALIP=$(dig ${PRODDOMAIN} +short)
ansible-playbook crin.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}"
```

To migrate from the old Jessie servers:

```bash
export SERVERNAME="crin4.webarchitects.co.uk"
export INTERNALIP=$(dig ${SERVERNAME} +short)
export PRODDOMAIN="crin.site"
export EXTERNALIP=$(dig ${PRODDOMAIN} +short)
ansible-playbook jessie-migrate.yml -i ${SERVERNAME}, -e "hostname=${SERVERNAME}"
```

# Drupal

The Drupal code is hosted at [the crin project on bitbucket](https://bitbucket.org/crin/crin).
